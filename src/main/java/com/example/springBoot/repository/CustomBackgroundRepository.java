package com.example.springBoot.repository;

import com.example.springBoot.entity.CustomBackgroundJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomBackgroundRepository extends JpaRepository<CustomBackgroundJob, Integer> {

}

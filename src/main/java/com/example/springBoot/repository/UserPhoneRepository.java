package com.example.springBoot.repository;


import com.example.springBoot.entity.UserPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserPhoneRepository extends JpaRepository<UserPhone, Integer> {
    List<UserPhone> findUsersByPhone(String phone);
}

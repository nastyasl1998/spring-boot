package com.example.springBoot.listeners;

import com.example.springBoot.entity.User;
import com.example.springBoot.services.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@ManagedResource
@Component
public class RabbitMqListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqListener.class);

    @Value("${DisplayEmail.property}")
    private Boolean flag;

    @Autowired
    private EmailService emailService;

    @ManagedAttribute
    public Boolean getFlag() {
        return flag;
    }

    @ManagedAttribute
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    @RabbitListener(queues = "queue-2-1")
    @Transactional
    public void receiveEmail(String email) {
        Optional<User> userOptional = emailService.getUserRequestByEmail(email);
        if (flag && !userOptional.isPresent()) {
            emailService.createUser(new User(email, "AUTOMATIC"));
            LOGGER.info("Email : " + email);
        }
    }
}


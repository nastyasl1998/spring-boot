package com.example.springBoot.controller;

import com.example.springBoot.entity.CustomBackgroundJob;
import com.example.springBoot.repository.CustomBackgroundRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.concurrent.locks.ReentrantLock;


@RestController
public class AppController {

    static final Logger LOGGER = LoggerFactory.getLogger(AppController.class);

    private ReentrantLock reentrantLock = new ReentrantLock();

    @Autowired
    private CustomBackgroundRepository customBackgroundRepository;

    @RequestMapping("/greeting")
    public void greeting(@RequestParam(value = "email") String email) {
        LOGGER.info("Received email: " + email);
    }

    @RequestMapping("/addingRecords")
    public void addingRecords() throws InterruptedException {
        for (int i = 1; i <= 5; i++) {
            reentrantLock.lock();
            customBackgroundRepository.save(new CustomBackgroundJob(LocalDateTime.now()));
            Thread.sleep(1000);
            reentrantLock.unlock();
        }
    }
}
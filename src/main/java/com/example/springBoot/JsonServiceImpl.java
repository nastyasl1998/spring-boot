package com.example.springBoot;

import com.example.springBoot.entity.User;
import com.example.springBoot.entity.UserPhone;
import com.example.springBoot.services.EmailService;
import com.example.springBoot.services.JsonService;
import com.example.springBoot.services.UserPhoneService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AutoJsonRpcServiceImpl
public class JsonServiceImpl implements JsonService {
    @Autowired
    private EmailService emailService;

    @Autowired
    private UserPhoneService userPhoneService;

    @Override
    public Boolean emailCheck(String email) {
        Boolean check;
        Optional<User> userOptional = emailService.getUserRequestByEmail(email);
        if (userOptional.isPresent()) {
            check = true;
        } else {
            check = false;
        }
        return check;
    }

    @Override
    public void addUser(String email) {
        Optional<User> userOptional = emailService.getUserRequestByEmail(email);
        if (userOptional.isEmpty()) {
            emailService.createUser(new User(email, "MANUAL"));
        }
    }

    @Override
    public void addPhone(String email, String phone) {
        Optional<User> userOptional = emailService.getUserRequestByEmail(email);
        if (userOptional.isEmpty()) {
            throw new IllegalArgumentException("Email does not exists");
        } else {
            userPhoneService.createUserPhone(new UserPhone(userOptional.get(), phone));
        }
    }

    @Override
    public Boolean emailAndPhoneCheck(String email, String phone) {
        boolean result = false;
        Optional<User> userOptional = emailService.getUserRequestByEmail(email);
        if (userOptional.isEmpty()) {
            throw new IllegalArgumentException("Email does not exists");
        } else {
            for (UserPhone userPhone : userOptional.get().getUserPhones()) {
                if (userPhone.getPhone().equals(phone)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public String numberToString(Integer number) {
        String str = Integer.toString(number);
        return str;
    }

    @Override
    public Integer calculateFactorial(Integer number) {
        int result = 1;
        for (int i = 1; i <= number; i++) {
            result = result * i;
        }
        return result;
    }

    @Override
    public Boolean deleteUserPhoneIfPhoneAlreadyExists(String email, String phone) {
        boolean check = false;
        Optional<User> userOptional = emailService.getUserRequestByEmail(email);
        if (userOptional.isPresent()) {
            for (UserPhone userPhone : userOptional.get().getUserPhones()) {
                if (userPhone.getPhone().equals(phone)) {
                    userPhoneService.deleteUserPhone(userPhone);
                    check = true;
                }
            }
        }
        return check;
    }

    @Override
    public List<String> findUsersByPhone(String phone) {
        List<UserPhone> list = userPhoneService.getUsersByPhone(phone);
        List<String> stringList = new ArrayList<>();
        for (UserPhone userPhone : list) {
            stringList.add(userPhone.getUser().getEmail());
        }
        return stringList;
    }

    @Override
    public List<String> findPhoneByBothEmail(String firstEmail, String secondEmail) {
        List<String> list = new ArrayList<>();
        Optional<User> firstUserOptional = emailService.getUserRequestByEmail(firstEmail);
        Optional<User> secondUserOptional = emailService.getUserRequestByEmail(secondEmail);
        for (UserPhone userPhone : firstUserOptional.get().getUserPhones()) {
            for (UserPhone userPhone1 : secondUserOptional.get().getUserPhones()) {
                if (userPhone.getPhone().equals(userPhone1.getPhone())) {
                    list.add(userPhone.getPhone());
                    if (list.size() == 5) {
                        return list;
                    }
                }
            }
        }
        return list;
    }
}


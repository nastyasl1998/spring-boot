package com.example.springBoot.services;

import com.example.springBoot.entity.User;
import com.example.springBoot.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class EmailService {

    private final UserRepository userRepository;

    public EmailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createUser(User user) {
        userRepository.save(user);
    }

    public Optional<User> getUserRequestByEmail(String email) {
        return userRepository.findByEmail(email);
    }

}

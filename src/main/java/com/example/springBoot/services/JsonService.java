package com.example.springBoot.services;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

import java.util.List;

@JsonRpcService("/check")
public interface JsonService {
    Boolean emailCheck(@JsonRpcParam(value = "email") String email);

    void addUser(@JsonRpcParam(value = "email") String email);

    void addPhone(@JsonRpcParam(value = "email") String email, @JsonRpcParam(value = "phone") String phone);

    Boolean emailAndPhoneCheck(@JsonRpcParam(value = "email") String email, @JsonRpcParam(value = "phone") String phone);

    String numberToString(@JsonRpcParam(value = "number") Integer number);

    Integer calculateFactorial(@JsonRpcParam(value = "number") Integer number);

    Boolean deleteUserPhoneIfPhoneAlreadyExists(@JsonRpcParam(value = "email") String email,
                                                @JsonRpcParam(value = "phone") String phone);

    List<String> findUsersByPhone(@JsonRpcParam(value = "phone") String phone);

    List<String> findPhoneByBothEmail(@JsonRpcParam(value = "firstEmail") String firstEmail,
                                      @JsonRpcParam(value = "secondEmail") String secondEmail);
}

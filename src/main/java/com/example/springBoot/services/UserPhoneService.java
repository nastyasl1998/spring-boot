package com.example.springBoot.services;


import com.example.springBoot.entity.UserPhone;
import com.example.springBoot.repository.UserPhoneRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserPhoneService {

    private final UserPhoneRepository userPhoneRepository;

    public UserPhoneService(UserPhoneRepository userPhoneRepository) {
        this.userPhoneRepository = userPhoneRepository;
    }

    public void createUserPhone(UserPhone userPhone) {
        userPhoneRepository.save(userPhone);
    }

    public void deleteUserPhone(UserPhone userPhone) {
        userPhoneRepository.delete(userPhone);
    }

    public List<UserPhone> getUsersByPhone(String phone) {
       return userPhoneRepository.findUsersByPhone(phone);
    }
}

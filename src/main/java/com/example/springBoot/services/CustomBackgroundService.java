package com.example.springBoot.services;

import com.example.springBoot.entity.CustomBackgroundJob;
import com.example.springBoot.repository.CustomBackgroundRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
public class CustomBackgroundService {

    private final CustomBackgroundRepository customBackgroundRepository;

    public CustomBackgroundService(CustomBackgroundRepository customBackgroundRepository) {
        this.customBackgroundRepository = customBackgroundRepository;
    }

    @Scheduled(fixedDelay = 25000)
    public void addCustomBackground() {
        LocalDateTime dateTime = LocalDateTime.now();
        customBackgroundRepository.save(new CustomBackgroundJob(dateTime));
    }
}

package com.example.springBoot.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USER_REQUEST")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "source")
    private String source;

    @OneToMany(mappedBy = "user")
    private List<UserPhone> userPhones;


    public List<UserPhone> getUserPhones() {
        return userPhones;
    }

    public void setUserPhones(List<UserPhone> userPhones) {
        this.userPhones = userPhones;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer Id) {
        this.id = Id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public User() {
    }

    public User(String email, String source) {
        this.email = email;
        this.source = source;
    }
}

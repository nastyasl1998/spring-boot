package com.example.springBoot.entity;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "CUSTOM_BACKGROUND_JOB")
public class CustomBackgroundJob {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public CustomBackgroundJob() {
    }

    public CustomBackgroundJob(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
